import imports from './modules/imports'
import getters from './getters'

// 统一导出各模块
export default {
  modules: { ...imports },
  getters
}
