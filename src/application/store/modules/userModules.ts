export default {
  state: {
    isClosedHeader: '这是测试文字',
    isClosedFooter: true,
    isClosedAside: true
  },
  mutations: {
    SET_BAR: (state:any, status: boolean) => {
      state.isClosedHeader = status
      state.isClosedFooter = status
      state.isClosedAside = status
    }
  }
}
