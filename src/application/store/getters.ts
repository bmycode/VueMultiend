export default {
  isClosedFooter: (state:any) => state.UserModules.isClosedFooter,
  isClosedHeader: (state:any) => state.UserModules.isClosedHeader,
  isClosedAside: (state:any) => state.UserModules.isClosedAside
}
