/**
 * 多端项目的总路由，路由拦截器可以在这配置。
 */
import Vue from 'vue'
import Router from 'vue-router'
// @ts-ignore
import DoctorRouter from '@/views/doctor/router/index'
// @ts-ignore
import NurseRouter from '@/views/nurse/router/index'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [].concat(...DoctorRouter, ...NurseRouter)
})
