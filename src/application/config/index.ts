export default {
  ApiUrl: {
    // 医生端管理后台接口地址
    doctor: {
      // 测试环境请求地址 (公共路径)
      devBaseUrl: 'http://www.geekhelp.cn/bmy/api',
      // 正式环境请求地址 (公共路径)
      proBaseUrl: 'http://www.geekhelp.cn/bmy/api2',
      specificUrl: {
        getClass: '/admin/getAllClass'
      }
    },
    // 护士端管理后台接口地址
    nurse: {
      // 测试环境请求地址 (公共路径)
      devBaseUrl: 'http://www.geekhelp.cn/bmy/api2',
      // 正式环境请求地址 (公共路径)
      proBaseUrl: 'http://www.geekhelp.cn/bmy/api23'
    }
  }
}
