import { BaseModel } from '../BaseModel'

export class DoctorHome extends BaseModel {
  /**
   * 请求医生端首页接口
   */
  public async getHomeData (params: Object | {}): Promise<Object> {
    try {
      return await this.$http.post({
        url: this.$config.ApiUrl.doctor.specificUrl.getClass,
        data: params
      })
    } catch (e) {
      return e.message
    }
  }
}
