import { Axios } from '../axios'
// @ts-ignore
import config from '@/config/index'

export class BaseModel {
  protected $http = new Axios()
  protected $config = config
}
