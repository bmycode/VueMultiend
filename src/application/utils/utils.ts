// @ts-ignore
import config from '@/config/index'

export class Utils {
  /**
   * 根据当前访问的后台地址的不同动态决定使用哪一端的对象
   * @constructor
   */
  public CurrentWebSiteUrl (): string {
    if (location.pathname.split('.')[0] === '/doctor') {
      return this.CheckTheEnvironment(config.ApiUrl.doctor)
    } else {
      return this.CheckTheEnvironment(config.ApiUrl.nurse)
    }
  }

  /**
   * CurrentWebSiteUrl 方法确定使用哪端后，再去读取该端下对应环境的接口基地址
   * @param url
   * @constructor
   */
  public CheckTheEnvironment (url: { devBaseUrl: string, proBaseUrl: string }) :string {
    if (process.env.NODE_ENV === 'development') {
      return url.devBaseUrl
    } else {
      return url.proBaseUrl
    }
  }
}
