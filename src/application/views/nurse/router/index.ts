import NurseIndex from '../pages/NurseIndex.vue'
import NurseAbout from '../pages/NurseAbout.vue'

export default [
  {
    path: '/NurseIndex',
    name: 'NurseIndex',
    component: NurseIndex
  },
  {
    path: '/NurseAbout',
    name: 'NurseAbout',
    component: NurseAbout
  }
]
