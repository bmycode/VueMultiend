import Home from '../pages/DoctorHome.vue'
import About from '../pages/DoctorAbout.vue'

export default [
  {
    path: '/index',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    component: About
  }
]
