import { InitVue } from './init'

class Nurse extends InitVue {
  /**
   * 初始化护士端管理后台
   */
  public newNurseVue (): void {
    const router = this.VueRouter
    const store = this.VueStore
    new this.Vue({
      router,
      store,
      render: (h: (arg0: any) => void):any => h(this.NurseVue)
    }).$mount(this.mountElement)
  }
}

(new Nurse()).newNurseVue()
