/**
 * axios 请求的参数接口
 */
export interface RequestParams {
  url: string,
  data: Object
}
