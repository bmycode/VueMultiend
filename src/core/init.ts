import Vue from 'vue'
import Vuex from 'vuex'
import { config } from './config'
import NurseVue from '@/views/nurse/nurse.vue'
import DoctorVue from '@/views/doctor/doctor.vue'
// @ts-ignore
import router from '@/router/router'
// @ts-ignore
import store from '@/store/index'

export class InitVue {
  // 需要挂载的App 节点
  protected mountElement: string = '#app'

  // 保存非Vue全局插件 n: 需要在Vue中使用的插件名, f：插件的类
  protected initOtherPlugsArray: Array<any> = config.NotVuePlugs
  // 需要被挂载的Vue插件
  protected initVuePlugsArray: Array<any> = config.VuePlugs

  protected ApiService: Array<any> = config.ApiService

  // 对外暴露 router，vuex
  protected VueRouter: any = router
  protected VueStore: any

  // 护士端入口
  protected NurseVue: any = NurseVue
  // 医生端入口
  protected DoctorVue: any = DoctorVue
  protected Vue: any = Vue

  public constructor () {
    Vue.config.productionTip = false
    this.initPlugs()
    this.initVuePlugs()
    this.initVuex()
  }

  /**
   * 1 统一挂载全局非 Vue 插件
   * 全局：在医生端 和 护士端 都需要使用的公共方法。
   *      需要先在 initOtherPlugsArray 定义。
   */
  public initPlugs (): void {
    // eslint-disable-next-line new-cap,no-return-assign
    this.initOtherPlugsArray.forEach(v => this.Vue.prototype[v['n']] = new (v['f'])())
    // 挂载非vue非类插件
    this.ApiService.forEach(v => { this.Vue.prototype[v['n']] = v['f'] })
  }

  /**
   * 统一挂载 Vue 插件
   * 全局：在医生端 和 护士端 都需要使用的公共方法。
   *      需要先在 initVuePlugsArray 定义。
   */
  public initVuePlugs (): void {
    this.initVuePlugsArray.forEach(v => this.Vue.use(v))
  }

  public initVuex (): void {
    this.VueStore = new Vuex.Store(store)
  }
}
