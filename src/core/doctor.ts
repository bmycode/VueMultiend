import { InitVue } from './init'
class Doctor extends InitVue {
  /**
   * 初始化护士端管理后台
   */
  public newDoctorVue (): void {
    const router = this.VueRouter
    const store = this.VueStore
    new this.Vue({
      router,
      store,
      render: (h: (arg0: any) => void):any => h(this.DoctorVue)
    }).$mount(this.mountElement)
  }
}

(new Doctor()).newDoctorVue()
