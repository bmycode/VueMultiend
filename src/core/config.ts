// @ts-ignore
import Service from '@/http/imports'
import vuex from 'vuex'

export class config {
  public static mountElement: string = '#app'
  // 需要被挂载的非Vue全局插件
  public static NotVuePlugs: Array<Object> = []
  public static ApiService: Array<Object> = [
    { n: '$http', f: Service }
  ]
  // 需要被挂载的Vue全局插件
  public static VuePlugs: Array<any> = [ vuex ]
}
