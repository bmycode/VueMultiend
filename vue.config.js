const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const isProduction = process.env.NODE_ENV === 'production'
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
  publicPath: './',
  filenameHashing: false,
  productionSourceMap: false,
  parallel: require('os').cpus().length > 1,
  css: {
    loaderOptions: {
      stylus: {
        data: `@import "@/assets/stylus/imports.styl";`
      }
    }
  },
  devServer: {
    hot: true,
    hotOnly: true,
    host: '0.0.0.0',
    port: 9000,
    compress: true,
    open: true,
    openPage: '/doctor.html#/index',
    overlay: {
      warnings: true,
      errors: true
    }
  },
  pages: {
    doctor: {
      entry: 'src/core/doctor.ts',
      template: 'public/doctor.html',
      filename: 'doctor.html',
      title: '医生端 管理后台',
      chunks: ['chunk-vendors', 'chunk-common', 'doctor'],
      CurrentEnvironment: isProduction,
      cdn: {
        css: [
          'https://cdn.bootcss.com/normalize/8.0.1/normalize.min.css'
        ],
        js: [
          'https://cdn.bootcss.com/vue/2.6.10/vue.runtime.min.js',
          'https://cdn.bootcss.com/vue-router/3.1.2/vue-router.min.js',
          'https://cdn.bootcss.com/vuex/3.1.1/vuex.min.js',
          'https://cdn.bootcss.com/axios/0.19.0-beta.1/axios.min.js'
        ]
      }
    },
    nurse: {
      entry: 'src/core/nurse.ts',
      template: 'public/nurse.html',
      filename: 'nurse.html',
      title: '护士端 管理后台',
      chunks: ['chunk-vendors', 'chunk-common', 'nurse'],
      CurrentEnvironment: isProduction,
      cdn: {
        css: [],
        js: [
          'https://cdn.bootcss.com/vue/2.6.10/vue.runtime.min.js',
          'https://cdn.bootcss.com/vue-router/3.1.2/vue-router.min.js',
          'https://cdn.bootcss.com/vuex/3.1.1/vuex.min.js',
          'https://cdn.bootcss.com/axios/0.19.0-beta.1/axios.min.js'
        ]
      }
    }
  },
  chainWebpack: config => {
    if (isProduction) {
      // 移除 prefetch，preload 插件 减少不必要的请求
      config.plugins.delete('prefetch')
      config.plugins.delete('preload')
    }

    // 配置路径别名，项目中引用资源的时候路径可以更简短
    // 但是ts文件在用的时候，编译器会报错，需要添加 @ts-ignore
    // 跳过编译器的错误。
    config.resolve.alias
      .set('@', resolve('src/application'))
      .set('@core', resolve('src/core'))
      .set('@assets', resolve('src/application/assets'))

    const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
    types.forEach(type => addStyleResource(config.module.rule('stylus').oneOf(type)))

  },
  configureWebpack: config => {
    if (isProduction) {
      /**
       * 去除这些公共包，让webpack不打包到项目源码的js中，减小项目js体积
       * 然后使用 pages 节点下的 cdn 数组里面的资源外链来引入。
       */
      config.externals = {
        'vue': 'Vue',
        'vuex': 'Vuex',
        'vue-router': 'VueRouter',
        'axios': 'axios'
      }

      // 开启 BundleAnalyzerPlugin
      return {
        plugins: [
          new BundleAnalyzerPlugin()
        ]
      }
    }
  }
}

function resolve (dir) {
  return path.join(__dirname, dir)
}

/**
 * 全局变量方法导入
 * @param rule
 */
function addStyleResource (rule) {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [
        path.resolve(__dirname, './src/application/assets/stylus/imports.styl'),
      ],
    })
}
